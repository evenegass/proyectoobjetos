package accesodatos;

import java.io.File;
import java.io.IOException;
import java.sql.*;

public class AccesoDato {

    private Connection conn;
    private Statement stmt;
     static final String PROYECTO_CONEXION = "Proyecto.txt";

    public AccesoDato(String driver, String strConexion) throws SQLException, Exception {
//se registra el driver del motor de la base de datos.
        Class.forName(driver);
//inicializa la conexión, pasando el string de conexión al método getConnection del DriverManager.
        conn = DriverManager.getConnection(strConexion);
//se inicializa el statement con la instancia de statement que retorna el método createStatement
        //de la clase conexión.
        stmt = conn.createStatement();
    }

    public AccesoDato(String driver, String url, String user, String pswd) throws SQLException, Exception {
// se registra el driver del motor de la base de datos.
        Class.forName(driver);
//inicializa la conexión, pasando el string de conexión al método getConnection del DirverManager
        conn = DriverManager.getConnection(url, user, pswd);
//iniciliaza el statement con la instancia de statement que retorna el método createStatement
        //de la calse conexión.
        stmt = conn.createStatement();
    }
//realiza las consultas de base de datos INSERT, UPDATE Y DELETE

    public void ejectuarSql(String query) throws SQLException {
        stmt.execute(query);
    }
//realiza las consultas de base de datos SELECT

    public ResultSet ejecutarQuery(String query) throws SQLException {
        return stmt.executeQuery(query);
    }
      public static void crearArchivo() throws IOException {
        File f = new File(PROYECTO_CONEXION);//constructor
        if (f.createNewFile()) {
            System.out.println("Archivo creado correctamente");
        } else {
            System.out.println("El archivo ya existe");
        }
    }
}
