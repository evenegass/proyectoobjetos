package accesodatos;

import static accesodatos.AccesoDato.PROYECTO_CONEXION;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

public class Conector {

    private static AccesoDato conectorBD = null;

    public static AccesoDato getConector()
            throws SQLException, Exception {
        if (conectorBD == null) {
            FileReader reader = new FileReader("PROYECTO.txt");
            BufferedReader buffer = new BufferedReader(reader);
            String BD = buffer.readLine();
            conectorBD = new AccesoDato("com.microsoft.sqlserver.jdbc.SQLServerDriver",
                    BD);
        }
        return conectorBD;
    }
   
}
